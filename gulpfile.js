var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss')
    //assumes that all scssfiles are stored in resources/assets/scss
    //uses the information in these files to complie from scss into rgular
    // css
      .scripts([
        'sweetalert-dev.js',
        'dropzone.js',
        'lity.js',
       	'jquery.js'
        //compiles from the orginal library into
        //our own custom library with bare tings

      ], 'public/js/libs.js')
        // this is out custom library with all javascript
        // lves in it

      .styles([
        'sweetalert.css',
        //'libs/basic.css',
        'dropzone.css',
        'lity.css'


      ], 'public/css/libs.css');
        //this is our custom css library with all levels in it
});