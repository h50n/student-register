<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();;
            $table->string('student');
            $table->decimal('amount',4,2);
            $table->integer('lesson_quantity');
            $table->string('method');
            $table->date('date');
            $table->timestamps();

            // $table->foreign('student_id')->references('id')->on('students')->onDelete('restrict|cascade')
            // ;
            // 
            // 
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
    
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('payments');
    }
}
