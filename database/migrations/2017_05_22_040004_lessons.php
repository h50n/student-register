<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lessons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student');
            $table->integer('student_id')->unsigned();;
            $table->string('lesson');
            $table->date('date');
            $table->rememberToken();
            $table->timestamps();


            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('lessons');    
     }
}
