<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Student extends Model
{

public $timestamps = true;

protected $table = 'students';

protected $dateFormat = 'd-m-Y H:i:s';

protected $fillable = [
    // change this for the fillable variables
    //	'a_liked',
    //	'b_liked',

];

//get all students
public static function showAllStudents()
{
	$students = DB::table('students')->get();

	return $students;

     //return view('students', ['students' => $users]);
     //

}

//show individual student

public static function showIndividualStudent($id)
{
	$student = DB::table('students')
			->where('id', $id)
			->first();

	return $student;
}

public static function showIndividualStudentLessons($id)
{
$student = DB::table('students')
            ->join('lessons', 'students.id', '=', 'lessons.student_id')->where('students.id',$id)
            ->get();

    return $student;




}

public static function showIndividualStudentPayments($id)
{

	DB::table('users')
            ->join('contacts', 'users.id', '=', 'contacts.user_id')->where('id',$id)
            ->get();



}

public static function totalLessonsBought($id)
{

	// join student and payments
	$totalLessonsBought = DB::table('students')
            ->join('payments', 'students.id', '=', 'payments.student_id')->where('students.id',$id)
            ->sum('lesson_quantity')
            ;

	

	// calculate sum of lesson amount where 
	// student id = student id 
	// ->sum('amount');
	

	//return total lessons bought
	return $totalLessonsBought;
	


}



//delete a student
public function deleteStudent($value='')
{
	# code...
}
//add a student
public function addStudent($value='')
{
	//create new student from the table 

}
//modify a student?

}
