<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Payment;
use App\Student;
use App\Lesson;
use DB;

class payments extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	$payments = Payment::getAllPayments();
    	$students = Lesson::getStudents();

    	return view('payments',compact('payments','students'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $this->validate($request, [
       
            'quantity' => 'required|integer',
            'amount' => 'required',
            'date' => 'date|required',
        ]);

        $amount = $request->input('amount');
        $date = $request->input('date');
        $quantity = $request->input('quantity');
        $method = $request->input('method');
      

      

        //we need to create 2 situations
        
        	//when there we have just the name
        	//on the payment page
        if ($request->input('student')){

          // $student = $request->student;
        $student = $request->get('student');

          $id = DB::table('students')
        		->where('student', $student)
        		->first();	

        	DB::table('payments')->insert([
        		'amount' => $amount,
        		'lesson_quantity' => $quantity,
        		'date' => $date,
        		'student_id'=> $id->id,
        		'student'=>$student,
        		'method'=>$method
        	]);
        		  return back()->withInput();	

        } else {

        //where we just have the id
        //on the student page	
               // if($request->input('id')){	

          $id = $request->input('id');	
          $student = DB::table('students')
        		->where('id', $id)
        		->first();	

        	DB::table('payments')->insert([
        		'amount' => $amount,
        		'lesson_quantity' => $quantity,
        		'date' => $date,
        		'student_id'=>$id,
        		'student'=>$student->student,
        		'method'=>$method
        	]);
    		
    // }


        return back()->withInput();	

	}

}




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
     
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    //this should actually be on lessons
    public function destroy($id)
    {
     
    	Payment::deletePayment($id);
return back()->withInput();


    }

    // public function showStudentPayments($id){

    // 	$payments = Payment::showStudentPayments($id);

    // 	return view('students'

    // }

    }