<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Student;
use App\Payment;
use DB;


class students extends Controller
{

    protected $dateFormat = 'd-m-Y H:i:s';

	public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $students = Student::showAllStudents();
        return view('students', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //required_if:anotherfield,value,...

// The field under validation must be present and not empty if the anotherfield field is equal to any value.
//         //
//         //
//         
//         
//         
        $this->validate($request, [
            'student' => 'required-if:student_id,false',
            'lesson' => 'required',
            'date' => 'date',
        ]);

        // if ($validator->fails()) {
        // return redirect('post/create')
        //             ->withErrors($validator)
        //             ->withInput();
        // }

        $lesson = $request->input('lesson');
        $date = $request->input('date');
      
      

        //we need to create 2 situations
        
        	//when there we have just the name
        if ($request->input('student')){

          // $student = $request->student;
        $student = $request->get('student');

          $id = DB::table('students')
        		->where('student', $student)
        		->first();	

        	DB::table('lessons')->insert([
        		'lesson' => $lesson,
        		'date' => $date,
        		'student_id'=> $id->id,
        		'student'=> $student 
        	]);


        	DB::table('students')->where('id', $id->id)->increment('lessons_had', 1);

        		return back();	

        } else {

        //where we just have the id
               // if($request->input('id')){	

          $id = $request->input('id');	
          $student = DB::table('students')
        		->where('id', $id)
        		->first();	

        	DB::table('lessons')->insert([
        		'lesson' => $lesson,
        		'date' => $date,
        		'student_id'=>$id,
        		'student'=>$student->student

        	]);
    		

    		DB::table('students')->where('id', $id)->increment('lessons_had', 1);
    // }


        return back();	




	}

}




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $studentDetails = Student::showIndividualStudent($id);
        // $student = "student";

        $student = Student::showIndividualStudentLessons($id);

        $studentPayments = Payment::showStudentPayments($id);

        $totalLessonsBought = Student::totalLessonsBought($id);

        $totalLessonsRemaining = $totalLessonsBought - $studentDetails->lessons_had; 
       
        return view('student', compact('student','studentDetails','studentPayments','totalLessonsBought','totalLessonsRemaining'));


        // 
        // return view('student');


        // $students = Student::showAllStudents();
        // return view('students', compact('students'));
        // 
        // 
     
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    //this should actually be on lessons
    public function destroy($id)
    {
        DB::table('students')
        	->where('id', '=', $id)
        	->delete();

        return back()->withInput();


    }

    public function submit($id)
    {	
		//
    }

    public function createNewStudent(Request $request)
    {
    	

        $this->validate($request, [
            'student' => 'required |unique:students,student',
    
        ]);


        DB::table('students')->insert([
    		'student' => $request->student
    		]);

    	return back();

    }}