<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});


// shows all lessons
Route::get('/lessons', function () {
    return view('lessons');
});

// shows all payments
Route::get('/payments', function () {
    return view('payments');
});

// Route::get('/students', function () {
//     return view('students');
// });

// shows a particular student and details
//Route::get('/student', function () {
  //  return view('student');
// });

// list of all the students
// Route::get('/students', function () {
//    return view('students');
// });

Route::get('/students','students@index');
Route::get('/lessons','lessons@index');
Route::get('/students/{id}','students@show');
Route::get('/payments','payments@index');



//Route::post('/students','students@show')
//
// Route::get('/students', function () {
//     return view('students');
// });


Route::post('/students','students@store');
Route::post('/payments','payments@store');

Route::post('/students/new','students@createNewStudent');

Route::delete('/students/{id}','students@destroy');
Route::delete('/lessons/{id}','lessons@destroy');
Route::delete('/payments/{id}','payments@destroy');





Route::auth();

Route::get('/home', 'HomeController@index');
