
@extends('layout')

@section('content')



</br>

</br>

</br>


// 1 // 
{{-- Student name --}}



<h1>{{ $studentDetails->student }}</h1>

profile pic

// 12 //  

//1- 5 //

<h4><strong>Total Lessons Bought:</strong> {{ $totalLessonsBought}}</h4>
</br>
<h4><strong>Total Lessons Had:</strong> {{$studentDetails->lessons_had }}</h4>
</br>
<h4><strong>Total Lessons Remaining:</strong> {{$totalLessonsRemaining}}</h4>


{{-- All Lessons --}}

{{-- All Payments --}}


<div class="container">
  <h2>Dynamic Pills</h2>
  <p>To make the tabs toggleable, add the data-toggle="pill" attribute to each link. Then add a .tab-pane class with a unique ID for every tab and wrap them inside a div element with class .tab-content.</p>
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#home">Home</a></li>
    <li><a data-toggle="pill" href="#menu1">Menu 1</a></li>
    <li><a data-toggle="pill" href="#menu2">Menu 2</a></li>
    <li><a data-toggle="pill" href="#menu3">Menu 3</a></li>
</ul>

<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>HOME</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
  </div>
  <div id="menu1" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
  </div>
  <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
  </div>
  <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
  </div>
</div>
</div>




<h2>Lessons</h2>




<table class="table table-dark">

    <thead>

        <td >ID</td>
        <td>Lesson</td>
        <td>Date</td>
        <td></td>

    </thead>

    @foreach( $student as $studentLesson)

    <tbody>
        <tr>
            <td>{{ $studentLesson->id}}</td>
            <td>{{ $studentLesson->lesson}}</td>
            <td>{{ date('d F Y', strtotime($studentLesson->date))}}</td>
            <td><form action="/lessons/{{$studentLesson->id}}" method="post">
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="Delete">
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Delete</button>
                </div>
            </form></td>

        </tr>
    </tbody>

    @endforeach

</table>


{{-- Add Lesson Form --}}


{{-- <div class="form">
    
    <div class="form-group">
        <label for="lesson">lesson:</label>
        <input type="text" name="lesson" id="lesson" class="form-control" value="{{ old('lesson') }}">
        
        <input type="date" name="date">
        <input type="submit">
    </div>
</form> --}}
{{-- 
    <div class="form-control">

        {{ Form::label($name, null, ['class' => 'control-label']) }}
        --}}


        <h2>Add New Lesson</h2>



        <div class="form-inline">

            {!! Form::open(['url' => '/students']) !!}

            Lesson: {!! Form::text('lesson', null, ['class' => 'form-control']) !!}
            Date: {!! Form::date('date', \Carbon\Carbon::now(),['class' => 'form-control'] ) !!}
            {!! Form::submit('add', null, ['class' => 'btn btn-primary']) !!}
            {!! Form::hidden('id',$studentDetails->id, ['class' => 'form-control'])!!}

            
            {!! Form::close() !!}

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            
        </div>

    </br>


    <h2>Payments</h2>


    <table class="table">

        <thead>
            <td>ID</td>
            <td>Amount</td>
            <td>Quantity</td>
            <td>Method</td>
            <td>Date</td>
            

        </thead>




        @foreach( $studentPayments as $studentPayment)

        <tbody>
            <tr>
                <td>{{ $studentPayment->id}}</td>
                <td>{{ $studentPayment->amount}}</td>
                <td>{{ $studentPayment->lesson_quantity}}</td>
                <td>{{ $studentPayment->method}}</td>
                <td>{{ date('d F Y', strtotime($studentPayment->date))}}</td>
                <td><form action="/payments/{{$studentPayment->id}}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="Delete">
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Delete</button>
                    </div>
                </form></td>

            </tr>
        </tbody>

        @endforeach

    </table>



    <h2>Add New Payment</h2>


    <div class="form-inline">

        {!! Form::open(['url' => '/payments']) !!}

        
        {{--  {!! Form::select('student', $students, ['class' => 'form-control']) !!} --}}




        Amount: {!! Form::number('amount', null, ['class' => 'form-control']) !!}
        Quantity: {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
        Method: {!! Form::text('method', null, ['class' => 'form-control']) !!}
        Date: {!! Form::date('date', \Carbon\Carbon::now(),['class' => 'form-control'] ) !!}
        {!! Form::submit('add', null, ['class' => 'btn btn-primary']) !!}
        {!! Form::hidden('id',$studentDetails->id, ['class' => 'form-control'])!!}

        
        {!! Form::close() !!}


        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif








        {{-- <div class="form-group">
            {{ Form::label($name, null, ['class' => 'control-label']) }}
            {{ Form::text($name, $value, array_merge(['class' => 'form-control'], $attributes)) }}
        </div> --}}


        {{-- <form class="form-inline">
          <div class="form-group">
            <label class="sr-only" for="exampleInputEmail3">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
        </div>
        <div class="form-group">
            <label class="sr-only" for="exampleInputPassword3">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
        </div>
        <div class="checkbox">
            <label>
              <input type="checkbox"> Remember me
          </label>
      </div>
      <button type="submit" class="btn btn-default">Sign in</button>
  </form>
  --}}









  @stop