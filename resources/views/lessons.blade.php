       
@extends('layout')

@section('content')


</br>

</br>

</br>


{{-- show all lessons --}}
<h2>Lessons</h2>

<table class="table">

    <thead>
            <td>Student</td>
            <td>Lesson</td>
            <td>Date</td>
            <td></td>

    </thead>


@foreach( $lessons as $lesson)

    <tbody>
        <tr>
            <td><a href="/students/{{$lesson->student_id}}">{{ $lesson->student}}<a/></td>
            <td>{{ $lesson->lesson}}</td>
            <td>{{ date('d F Y', strtotime($lesson->date))}}</td>
            <td><form action="/lessons/{{$lesson->id}}" method="post">
{!! csrf_field() !!}
    <input type="hidden" name="_method" value="Delete">
    <div class="form-group">
        <button type="submit" class="btn btn-default">Delete</button>
    </div>
</form></td>

        </tr>
    </tbody>

@endforeach

</table>

{{-- Add Lesson Form --}}


{{-- <div class="form">
    
    <div class="form-group">
        <label for="lesson">lesson:</label>
                        <input type="text" name="lesson" id="lesson" class="form-control" value="{{ old('lesson') }}">
    
        <input type="date" name="date">
  <input type="submit">
  </div>
</form> --}}
{{-- 
<div class="form-control">

{{ Form::label($name, null, ['class' => 'control-label']) }}
 --}}


 <h2>Add New Lesson</h2>


<div class="form-inline">

{!! Form::open(['url' => '/students']) !!}

    
   {{--  {!! Form::select('student', $students, ['class' => 'form-control']) !!} --}}


    <select class="form-control" name="student">
        @foreach($students as $key => $value)
             <option value="{{$value}}">{{$value}}</option>
        @endforeach
  </select>

    Lesson: {!! Form::text('lesson', null, ['class' => 'form-control']) !!}
    Date: {!! Form::date('date', \Carbon\Carbon::now(),['class' => 'form-control'] ) !!}
    {!! Form::submit('add', null, ['class' => 'btn btn-primary']) !!}
    {{-- {!! Form::hidden('id',$lessons->id, ['class' => 'form-control'])!!} --}}

    
{!! Form::close() !!}



@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




@stop