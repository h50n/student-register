
@extends('layout')

@section('content')


</br>

</br>

</br>



<h2>Students</h2>

<table class="table">

    <thead>
        <th>
            <td></td>
        </th>
    </thead>

@foreach($students as $student)

    <tbody>
        <tr>
            <td>{{ $student->id }}</td>
            {{-- student name links to individual page --}}

            <td><a href="/students/{{$student->id}}">{{ $student->student }}</a></td>
            {{-- delete button  links to destroy page--}}


<td><form action="/students/{{$student->id}}" method="post">
{!! csrf_field() !!}
    <input type="hidden" name="_method" value="Delete">
    <div class="form-group">
        <button type="submit" class="btn btn-default">Delete</button>
    </div>
</form></td>
        </tr>
    </tbody>

@endforeach

</table>


{{-- New Student Form name --}}

 {{-- <td><a href="/students/{{$student->id}}">{{ $student->student }}</a></td> --}}
{{-- 
 <td>{{ $student->student }}</td>

  <td><form action="/students/{{$studentLesson->id}}" method="post"> --}}


<h2>Add Student</h2>

  <div class="form-inline">

{!! Form::open(['url' => '/students/new']) !!}

    Name: {!! Form::text('student', null, ['class' => 'form-control']) !!}
    {!! Form::submit('add', null, ['class' => 'btn btn-primary']) !!}

    
{!! Form::close() !!}


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
</div>

@stop
