
@extends('layout')

@section('content')


</br>

</br>

</br>



{{-- All payments --}}

<h2>Payments</h2>

<table class="table">

    <thead>
            <td>Student</td>
            <td>Lesson Quantity</td>
            <td>Amount</td>
            <td>Date</td>
            <td>Method</td>
            <td></td>

    </thead>


@foreach( $payments as $payment)

    <tbody>
        <tr>
            <td><a href="/students/{{$payment->student_id}}">{{ $payment->student}}<a/></td>
            <td>{{ $payment->lesson_quantity}}</td>
            <td>{{ $payment->amount}}</td>
            <td>{{ date('d F Y', strtotime($payment->date))}}</td>
            <td>{{ $payment->method}}</td>
            <td><form action="/payments/{{$payment->id}}" method="post">
{!! csrf_field() !!}
    <input type="hidden" name="_method" value="Delete">
    <div class="form-group">
        <button type="submit" class="btn btn-default">Delete</button>
    </div>
</form></td>

        </tr>
    </tbody>

@endforeach

</table>



<h2>Add New Payment</h2>


<div class="form-inline">

{!! Form::open(['url' => '/payments']) !!}

    
   {{--  {!! Form::select('student', $students, ['class' => 'form-control']) !!} --}}


    <select class="form-control" name="student">
        @foreach($students as $key => $value)
             <option value="{{$value}}">{{$value}}</option>
        @endforeach
  </select>

    Amount: {!! Form::number('amount', null, ['class' => 'form-control','step'=>'0.01']) !!}
    Quantity: {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
    Method: {!! Form::text('method', null, ['class' => 'form-control']) !!}
    Date: {!! Form::date('date', \Carbon\Carbon::now(),['class' => 'form-control'] ) !!}
    {!! Form::submit('add', null, ['class' => 'btn btn-primary']) !!}
    {{-- {!! Form::hidden('id',$lessons->id, ['class' => 'form-control'])!!} --}}


    
{!! Form::close() !!}


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


@stop

